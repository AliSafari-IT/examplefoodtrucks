package com.verboven.springbootjpamulti.repository;

import com.verboven.springbootjpamulti.entity.Foodtruck;
import com.verboven.springbootjpamulti.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Collection<Product> findByFoodtruckId(long id);
}
