package com.verboven.springbootjpamulti.repository;

import com.verboven.springbootjpamulti.entity.Foodtruck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodtruckRepository extends JpaRepository<Foodtruck, Long> {
}
