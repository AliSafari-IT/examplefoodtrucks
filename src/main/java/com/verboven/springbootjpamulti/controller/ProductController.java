package com.verboven.springbootjpamulti.controller;

import com.verboven.springbootjpamulti.entity.Foodtruck;
import com.verboven.springbootjpamulti.entity.Product;
import com.verboven.springbootjpamulti.exception.ResourceNotFoundException;
import com.verboven.springbootjpamulti.repository.FoodtruckRepository;
import com.verboven.springbootjpamulti.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    FoodtruckRepository foodtruckRepository;

    @GetMapping("/foodtrucks/{id}/products")
    public Collection<Product> getAllProductsByFoodtruckId(@PathVariable Long id){
        return productRepository.findByFoodtruckId(id);
    }

    @PostMapping("/foodtrucks/{id}/products")
            public Product createProduct(@Valid @RequestBody Product product, @PathVariable Long id){
    Foodtruck foodtruck = foodtruckRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", id));
    product.setFoodtruck(foodtruck);
    return productRepository.save(product);
    }

    @PutMapping("/foodtrucks/{foodtruckId}/products/{productId}")
    public Product updateProduct(@PathVariable (value = "foodtruckId") Long foodtruckId, @PathVariable (value= "productId") Long productId, @Valid @RequestBody Product productDetails){
        Foodtruck foodtruck = foodtruckRepository.findById(foodtruckId)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", foodtruckId));
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));

        product.setName(productDetails.getName());
        product.setType(productDetails.getType());

        return productRepository.save(product);
    }

    @DeleteMapping("/foodtrucks/{foodtruckId}/products/{productId}")
    public ResponseEntity<?> deleteProduct(@PathVariable (value = "foodtruckId") Long foodtruckId, @PathVariable (value= "productId") Long productId){
        Foodtruck foodtruck = foodtruckRepository.findById(foodtruckId)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", foodtruckId));
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));

        productRepository.delete(product);
        return ResponseEntity.ok().build();
    }
}
