package com.verboven.springbootjpamulti;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.verboven.springbootjpamulti.entity.Event;
import com.verboven.springbootjpamulti.entity.Foodtruck;
import com.verboven.springbootjpamulti.entity.Product;
import com.verboven.springbootjpamulti.entity.ProductType;
import com.verboven.springbootjpamulti.repository.FoodtruckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class FoodtruckControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private FoodtruckRepository foodtruckRepository;

    private ObjectMapper mapper = new ObjectMapper();

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void givenFoodtrucks_whenGetAllFoodtrucks_thenReturnJsonArray()
            throws Exception {

        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        foodtruckRepository.save(truck);

        mvc.perform(get("/foodtrucks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name").value(truck.getName()))
                .andExpect(jsonPath("$[0].products[0].name").value(product.getName()));
    }


    @Test
    public void givenFoodtruck_whenGetFoodtruckById_thenReturnFoodtruck()
            throws Exception {
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        foodtruckRepository.save(truck);

        mvc.perform(get("/foodtrucks/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(truck.getName()))
                .andExpect(jsonPath("$.products[0].name").value(product.getName()));

        mvc.perform(get("/foodtrucks/{id}",100001L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Hotdogs Jef"));
    }

    @Test
    public void givenFoodtruck_whenGetFoodtruckEvents_thenReturnJsonArray()
            throws Exception {

        mvc.perform(get("/foodtrucks/200001/events")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Zomerpret"));

    }

    @Test
    public void givenFoodtruck_whenGetFoodtruckFee_thenReturnString()
            throws Exception {
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        foodtruckRepository.save(truck);

        mvc.perform(get("/foodtrucks/{id}/fee", 1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("Your fee is €" + truck.getStaffnumber()*10));
    }

    @Test
    public void whenCreateFoodtruck_thenReturnFoodtruck()
            throws Exception{
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);
        Event event = new Event();
        event.setLocation("Testlocation");
        event.setName("Testevent");
        event.addFoodtruck(truck);
        truck.addEvent(event);

        mvc.perform(post("/foodtrucks")
                .content(convertObjectToJsonBytes(truck))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(truck.getName()))
                .andExpect(jsonPath("$.products[0].name").value(product.getName()));

        Foodtruck foundTruck = foodtruckRepository.findById(1L).get();
        assertThat(foundTruck.getName()).isEqualTo(truck.getName());
    }

    @Test
    public void givenFoodtruck_whenUpdateFoodtruck_thenReturnFoodtruck()
            throws Exception{

        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        foodtruckRepository.save(truck);

        truck.setName("Edittruck");
        truck.setStaffnumber(4);

        mvc.perform(put("/foodtrucks/{id}",truck.getId())
                .content(convertObjectToJsonBytes(truck))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Edittruck"));

        Foodtruck foundTruck = foodtruckRepository.findById(truck.getId()).get();
        assertThat(foundTruck.getName()).isEqualTo("Edittruck");
    }

    @Test
    public void whenDeleteFoodtruck_thenStatus200()
            throws Exception{
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        foodtruckRepository.save(truck);

        mvc.perform(delete("/foodtrucks/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(foodtruckRepository.findById(1L).isPresent()).isEqualTo(false);
    }

}
